﻿


> Written with [StackEdit](https://stackedit.io/).
> 

## Tipos de web hosting

> En servicio de  **alojamiento web (_web hosting_)**  es un tipo de  **alojamiento en Internet**  que posibilita a usuarios individuales y empresas hacer  **accesible su sitio web**  a través de la web de Internet (World Wide Web).

![770px-Floridaserversfront1](https://javiergarciaescobedo.es/images/stories/despliegue_web/01_implantacion/770px-Floridaserversfront1.jpg)Las empresas que ofrecen este tipo de alojamiento proporcionan un determinado  **espacio de almacenamiento**  en uno o varios de sus  **servidores**  para ser usado por los clientes. En dichos servidores pueden estar **instaladas y configuradas** previamente las aplicaciones que permiten realizar la función de  **servidor web o servidor de aplicaciones**, así como otras herramientas por ejemplo para la  **transferencia de archivos**  o algún servidor de  **base de datos**  como MySQL. De esta manera, el cliente se puede despreocupar del proceso de instalación y configuración de esos servicios.

También es frecuente que se proporcione un  **panel de control**  para la gestión por parte del cliente del servidor web, instalar módulos y otros servicios adicionales como correo electrónico, o la ejecución de scripts que automatizan el proceso de instalación de gestores de contenidos como Joomla, Drupal, etc.

Los principales  **tipos**  de alojamientos web son los siguientes:

-   **Servicio de alojamiento web gratuito (_Free web hosting service_)**: es ofrecido por diferentes empresas con servicios limitados (espacio de almacenamiento, soporte de lenguajes para webs dinámicas, bases de datos, etc). Se suelen utilizar servidores compartidos por un alto número de usuarios.
-   **Servicio de alojamiento web compartido (_Shared web hosting service_)**: Cada sitio web es alojado en un mismo servidor que otros sitios web, que puede oscilar desde pocos cientos a miles. Todos los sitios alojados en un mismo servidor compartirán sus recursos como su memoria RAM y la CPU, por lo que este tipo de servicio es bastante básico. Puede ser un servicio gratuito o de pago con cuotas mensuales o anuales.
-   **Servidor dedicado virtual (_Virtual dedicated server_)**: También se conoce como Servidor privado virtual (_Virtual private server - VPS_). Consiste en dividir los recursos (RAM, CPU, ...) de la máquina que hace de servidor, en servidores virtuales. Los clientes pueden tener acceso de administrador a su propio servidor virtual para realizar cualquier tipo de configuración o instalación de aplicaciones en él.
-   **Servidor dedicado (_Dedicated hosting service_)**: El cliente tiene su propia máquina que hará de servidor web, ubicada físicamente en el centro de datos de la empresa que ofrece el servicio. De esta manera, se tiene permisos de administrador sobre toda la máquina. La seguridad y el mantenimiento del servidor puede realizarse por parte de la empresa que ofrece el servicio o por parte del cliente para abaratar el coste.
-   **Servidor propio**: Cualquier persona o empresa puede tener una máquina situada en su  **propia residencia o ubicación de la empresa**, para que se dedique a ofrecer el servicio de alojamiento web, siempre que se disponga de una conexión a Internet que debería tener un alto  **ancho de banda**. En ocasiones se utilizan  **máquinas antiguas**, ya que las aplicaciones que realizan las funciones de servidor web no tienen muchos requerimientos. Para que los usuarios puedan acceder al sitio web alojado en el servidor es conveniente disponer de una dirección  **IP pública fija**, o usar un  **servidor DNS dinámico**, que permita a los usuarios localizar al servidor. En la configuración del router que da la conexión a Internet se debe configurar el  **redireccionamiento de puertos**, de manera que las peticiones recibidas por el puerto 80 se redirijan a la dirección IP de la red local que posee la máquina.

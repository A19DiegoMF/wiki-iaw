# Generación de certificados digitales autofirmados

## Introducción

Un **certificado autofirmado** puede ser útil para implementar SSL en entornos de desarrollo/testeo, o para el caso en que no se cuente con presupuesto suficiente para comprar uno firmado por una autoridad certificante de confianza (aunque algunas autoridades certificantes como namecheap ofrecen certificados económicos). **Un certificado autofirmado no sirve para garantizar la identidad de un servidor, pero al menos provee un canal de comunicación seguro**. Generar un certificado autofirmado también sirve para entender el proceso de creación y firma de un certificado y sus diferentes componentes.

Antes de comenzar, crear un directorio donde guardar certificados y sus claves:

```
mkdir ~/certs
cd ~/certs
```

## Primer paso (generación de una solicitud de emisión de certificado)

Primero se debe generar una solicitud de firma de certificado ([CSR, Certificate Signing Request](https://en.wikipedia.org/wiki/Certificate_signing_request)). Esta contiene la información del certificado junto con la clave pública, y es lo que envía a las autoridades certificantes para ser firmada (generar el certificado de confianza). El archivo CSR utiliza generalmente el formato binario [PKCS #10](https://tools.ietf.org/html/rfc2986).

Con el siguiente comando, se genera una clave privada y una solicitud de firma de certificado con su correspondiente clave pública:

```
openssl req -new -nodes -keyout privada.key -out solicitud.csr -days 3650
```

Este comando solicita al usuario ingresar por entrada estándar cada uno de los campos del certificado, siendo el más importante el `common name (CN)`, el cual debe coincidir con el nombre de host calificado del servidor (por ejemplo `www.miblog.me`) si se va a utilizar para proveer SSL en un servidor Web (HTTPS).

El parámetro `-keyout` especifica el nombre de archivo para la clave privada, mientras que `-nodes` indica que no se proteja con contraseña. Esto se hace por conveniencia ya que, de lo contrario, cada vez que se reinicie el servicio (que utilice dicho certificado) será necesario ingresar la contraseña (passphrase) para desencriptar la clave privada (lo cual entorpece la administración).

Por otro lado, el parámetro `-out` indica el nombre de archivo para el CSR y `-days` indica la duración de la validez del CSR. No confundir `-days` con el período de validez del certificado una vez emitido. Este número sólo indica la cantidad de días que tiene la autoridad certificante para firmar la solicitud (luego del cual será necesario generar una nueva solicitud).

Al finalizar la ejecución se han generado dos archivos: `privada.key` que corresponde a la clave privada, y `solicitud.csr` que corresponde a la solicitud de emisión del certificado y contiene la clave pública.

Por supuesto, **al no proteger la clave privada con contraseña, será necesario ajustar sus permisos de lectura adecuadamente**:

```
chmod 400 privada.key
```

De esta forma se mantiene a la clave privada en secreto:

```
# ls
-rw-r--r-- 1 root root 1.1K Oct  1 11:15 solicitud.csr
-r-------- 1 root root 1.7K Oct  1 11:15 privada.key
```

El formato de archivo PKCS #10 se codifica en `base64`:

```
cat solicitud.csr 
-----BEGIN CERTIFICATE REQUEST-----
MIICijCCAXICAQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUx
ITAfBgNVBAoMGEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDCCASIwDQYJKoZIhvcN
AQEBBQADggEPADCCAQoCggEBANvBvefKe/s9ro1B6bQJwGKyy7wuDVUyziCJa0RI
b8AxSkPGAZL006ruZjtBrZHET7DRr0MfNFfItz4VTpg2tcwkJfJRLeEqXv8GfIDA
CMjVWmLUAH1ATE+PZ2oD7Fxe81/dgwNHZUwmsE71DccySZRKIq9zrkKIoOPCsvsB
SkzomuH+JaJXwTiPEJVuQq7C/EerCJzXqk8yGYyqZGiGSMxwXjVkuytX+UCZt2A7
UxbES6rzjR6yEwOtMOVrQTBZipdkT0yF14ycH/xYYVUF8qqieoo0ZjlEmXZ0PO/d
LbHGkSJZOO1qvkJesX15G+xUAxGrJKO2IflIqtpLdJl9r/0CAwEAAaAAMA0GCSqG
SIb3DQEBCwUAA4IBAQBh/G4XsO/mk1sTHQkQORlzTdSXI7GR5QIOOOHxtkgYd7dt
LW/k/m9iBeKuVwamtQrXVLfDqrOIvIxE4NMu0Wkmw9ZJbcSqo0Ppp4cIgNV1RazR
fnwhXgpoLTptQl8gpJObWENWKx+L8FgEnv6VlE2fD+Vh409oYQPn1hsocWlWDs/h
1Omd43vBCAKKMmSu7Mh4ZeAZqTYe2gpibtBf/eexubPLC6zUkE+51fPXyQvSVoS3
2vDq2W1FIQbOp7k3FnwiZ3dCKaBEo5ei2WbNF9Mo77FXv6eeb05ZcyRAljcx0U3f
sUQRgio+A3xKneIW1+gLrftLb2g9hQCUoJpAteme
-----END CERTIFICATE REQUEST-----
```

Es posible decodificarlo utilizando el comando `base64`, aunque es un formato es binario:

`grep -v "^--" solicitud.csr | base64 -d | tr -dc '[:print:]'`

Daría este resultado:

```
00r0E10UAU10USome-State1!0UInternet Widgits Pty Ltd0"0*H0{=Ab.U2 kDHo1JCf;AOC4W>N6$%Q-*^|Zb}@LOgj\^_GeL&N2IJ"sBJL%W8nBGO2dhHp^5d+W@`;SK0kA0YdOLXaUz4f9Dvt<-"Y8jB^}yT$!HKt}0*Han[9sM#8Hwm-oobWTD.i&ImCuE~|!^h-:mB_ XCV+XMaOha(qiV{2dxe6bn_OVmE!7|"gwB)Df(WoNYs$@71MD*>|JKoh=@
```

Para interpretarlo correctamente es mejor utilizar el siguiente comando: 

`openssl req -subject -noout -in solicitud.csr`

Con eso verificamos que la solicitud sea correcta.

## Segundo paso (autofirmado)

A continuación es posible enviar el CSR a una autoridad certificante para que lo firme y emita el certificado correspondiente. Sin embargo es posible también firmar la solicitud con su misma clave privada, generando así un certificado "autofirmado". Para ello ejecutar:

```
openssl x509 -req -days 3650 -in solicitud.csr -signkey privada.key -out certificado.crt
```

El subcomando `x509` se utiliza para firmar solicitudes y verificar certificados. La opción `-req` indica que se intenta crear un certificado a partir de una solicitud desde el archivo especificado por `-in`. El certificado será guardado con el nombre que indica `-out` y será válido por `-days` días a partir de la fecha actual del servidor. Por último, el parámetro `-signkey` indica la clave con la que será firmado el certificado, la cual (en este ejemplo) es la propia clave privada del certificado emitido. En otro caso, sería la clave privada de la entidad de certificación.

Con este comando se genera el certificado autofirmado `certificado.crt`.

Es posible verificar su validez mediante:

`openssl x509 -subject -issuer -enddate -noout -in certificado.crt`

Queda claro que el certificado es autofirmado al notar que el subject ("asunto" o "sujeto" del certificado) coincide con el issuer (emisor del certificado, quien lo firma).

El certificado se emite en el formato binario [DER (Distinguished Encoding Rules)](https://tools.ietf.org/html/rfc5485#ref-X.690), codificado también en `base64`. Al igual que el CSR, es posible decodificarlo.




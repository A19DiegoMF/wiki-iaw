﻿


> Written with [StackEdit](https://stackedit.io/).

## > APACHE

## Conceptos básicos

-   Servidor web HTTP de código abierto
-   Plataformas Linux, Windows, Mac y otras
-   Implementa el protocolo HTTP/1.1
-   Se desarrolla dentro del proyecto HTTP Server (httpd) de la Apache Software Fundation
-   Apache es el servidor HTTP más usado
-   Extremadamente estable

## Ventajas de usar Apache2

-   Modular
-   Código abierto
-   Multi-plataforma
-   Extensible
-   Popular (fácil conseguir ayuda/soporte)

## Instalación en Debian/Ubuntu

Para instalar el servidor web Apache en sistemas GNU/Linux Debian/Ubuntu y derivados, ejecutamos como administrador:

```
apt-get install apache2

```

Para controlar el servicio apache2 podemos usar (para más  [información](http://httpd.apache.org/docs/2.4/es/stopping.html)):

```
apache2ctl [-k start|restart|graceful|graceful-stop|stop]

```

La opción  `graceful`  es un reinicio suave, se terminan de servir las peticiones que están establecidas y cuando se finaliza se hace una reinicio del servidor.

Con esta herramienta podemos obtener también más información del servidor:

-   `apache2ctl -t`  : Comprueba la sintaxis del fichero de configuración.
-   `apache2ctl -M`  : Lista los módulos cargados.
-   `apache2ctl -S`  : Lista los sitios virtuales y las opciones de configuración.
-   `apache2ctl -V`  : Lista las opciones de compilación

Evidentemente el servidor está gestionado por  `systemd`, por lo tanto para controlar el arranque, reinicio y parada del servicio utilizaremos la siguiente instrucción:

```
systemctl [start|stop|restart|reload|status] apache2.service

```

## Instalación en Windows

-   [Using Apache HTTP Server on Microsoft Windows](https://httpd.apache.org/docs/2.4/platform/windows.html)
`

## Configuración de apache
En algunas distribuciones de linux existe un fichero de configuración único, mientras que en Debian/Ubuntu la configuación de los ficheros está distribuida, con un fichero de configuracion principal `/etc/apache2/apache2.conf`
En ese fichero se incluyen los ficheros que forman parte de la configuración de Apache2:

**Configuración de módulos**

IncludeOptional mods-enabled/*.load

IncludeOptional mods-enabled/*.conf
...

**Configuración de puertos**

Include ports.conf

...
**Configuración de sitios virtuales**

IncludeOptional sites-avaiable/*.conf

IncludeOptional sites-enabled/*.conf

``

-   Los ficheros que se añaden guardados en el directorio  `mods-enabled`  correponden a los módulos activos.
-   Los ficheros añadidos del directorio  `sites-enabled`  corresponden a la configuración de los sitios virtuales activos.
-   Del directorio  `conf-enabled`  añadimos ficheros de configuración adicionales.
-   Por último en el fichero  `ports.conf`  se especifica los puertos de escucha del servidor.

## Opciones de configuración para los servidores virtuales

Por defecto se indican las opciones de configuración del directorio  `/var/www`  y de todos sus subdirectorios, por lo tanto los  `DocumentRoot`  de los virtual host que se crean deben ser subdirectorios del este directorio, por lo tanto encontramos en el fichero  `/etc/apache2/apache2.conf`  lo siguiente:

```
<Directory /var/www/>
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>

```

Podemos indicar como directorio raíz de nuestros virtual host otro directorio (tenemos que descomentar):

```
#<Directory /srv/>
#    Options Indexes FollowSymLinks
#    AllowOverride None
#    Require all granted
#</Directory>

```

## Añadir nueva configuración

Si tenemos configuración adicional para nuestro servidor podemos guardarla en un fichero (por ejemplo  `prueba.conf`) dentro del directorio  `/etc/apache2/conf-available`. Para añadir dicho fichero de configuración a la configuración general del servidor usamos la instrucción:

```
# a2enconf prueba

```

Esta instrucción crea un enlace simbólico en el directorio  `/etc/apache2/conf-enabled`. Para desactivar una configuración usamos:

```
# a2disconf prueba 

```
También:

 - a2enmod, a2dismod para activar o desactivar la configuración de módulos
 - a2ensite, a2dissite para añadir o quitar sitios virtuales.
 
## Variables de entorno de Apache

El servidor HTTP Apache HTTP ofrece un mecanismo para almacenar información en variables especiales que se llaman variables de entorno. Esta información puede ser usada para controlar diversas operaciones como por ejemplo, almacenar datos en ficheros de registro (`log files`) o controlar el acceso al servidor. Podemos encontrar estas variables definidas en el fichero  `/etc/apache2/envvars`.

Estudiemos algunas directivas que podemos encontrar en  `/etc/apache2/apache2.conf`:

## Directivas de control de la conexión

-   [`Timeout`](http://httpd.apache.org/docs/2.4/mod/core.html#timeout): define, en segundos, el tiempo que el servidor esperará por recibir y transmitir durante la comunicación.  `Timeout`  está configurado por defecto a 300 segundos, lo cual es apropiado para la mayoría de las situaciones.
-   [`KeepAlive`](http://httpd.apache.org/docs/2.4/mod/core.html#keepalive): Define si las conexiones persistentes están activadas. Por defecto están activadas.
-   [`MaxKeepAliveRequests`](http://httpd.apache.org/docs/2.4/mod/core.html#maxkeepaliverequests): Establece el número máximo de peticiones permitidas por cada conexión persistente. Por defecto está establecido como 100. Si hacen falta más se cierra la conexión TCP/IP y se abre otra nueva
-   [`KeepAliveTimeout`](http://httpd.apache.org/docs/2.4/mod/core.html#keepalivetimeout): Establece el número de segundos que el servidor esperará tras haber dado servicio a una petición, antes de cerrar la conexión. Por defecto 5 segundos. Es un valor bajo por cuestiones de rendimiento.

## Otras directivas

-   [`User`](http://httpd.apache.org/docs/2.4/mod/mpm_common.html#user): define el usuario que ejecuta los procesos de Apache2.
-   [`Group`](http://httpd.apache.org/docs/2.4/mod/mpm_common.html#group): define el grupo al que corresponde el usuario.
-   [`LogLevel`](http://httpd.apache.org/docs/2.4/mod/core.html#loglevel): Controla el nivel de información que se guarda en los ficheros de registro o logs.
-   [`LogFormat`](http://httpd.apache.org/docs/2.4/mod/mod_log_config.html#logformat): Controla el formato de información que se va a guardar en los ficheros logs.
-   [`Directory`](http://httpd.apache.org/docs/2.4/mod/core.html#directory)  o  [`DirectoryMatch`](http://httpd.apache.org/docs/2.4/mod/core.html#directorymatch): Declara un contexto para un directorio y todos sus directorios.
-   [`Files`](http://httpd.apache.org/docs/2.4/mod/core.html#files)  o  [`FilesMatch`](http://httpd.apache.org/docs/2.4/mod/core.html#filesmatch): Declara un contexto para un conjunto de ficheros.


## Virtual Hosts
El servidor web Apache 2.4 se instala por defecto con la configuración de un servidor virtual. La configuración de este sitio la podemos encontrar en:

```
/etc/apache2/sites-available/000-default.conf

```

Y por defecto este sitio virtual está habilitado, por lo que podemos comprobar que existe un enlace simbólico a este fichero en el directorio  `/etc/apache2/sites-enabled`:

```
lrwxrwxrwx 1 root root   35 Oct  3 15:24 000-default.conf -> ../sites-available/000-default.conf

```

Una de las directivas más importantes que nos encontramos en el fichero de configuración es  [`DocumentRoot`](https://httpd.apache.org/docs/2.4/mod/core.html#documentroot)  donde se indica el directorio donde van a estar guardados los ficheros de nuestro sitio web, los ficheros que se van a servir. En la configuración del virtual host por defecto el directorio es:

```
/var/www/html

```

En el fichero de configuración general  `/etc/apache2/apache2.conf`  nos encontramos las opciones de configuración del directorio padre del indicado en la directiva  `DocumentRoot`  (suponemos que todos los host virtuales van a estar guardados en subdirectorios de este directorio):

```
...
<Directory /var/www/>
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>
...

```

Por lo tanto podemos acceder desde un navegador a la ip de nuestro servidor (también podemos usar un nombre del servidor) y accederemos a la página de bienvenida del servidor que se encuentra en:

```
/var/www/html/index.html

```

Por defecto los errores de nuestro sitio virtual se guardan en  `/var/log/apache2/error.log`  y los accesos a nuestro servidor se guardan en  `/var/log/apache2/access.log`.



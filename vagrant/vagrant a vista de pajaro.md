﻿


> Written with [StackEdit](https://stackedit.io/).
> ## Todos los comandos de Vagrant

-   En la consola invocamos
    -   `vagrant`
-   Obtenemos un listado de comandos
    -   `halt, help, init, provision, reload, snapshot, ssh, status, suspend, up`
-   Obtenemos ayuda
    -   `vagrant [command] -h`
-   Ayuda: `vagram box --help`
-                vagram box add --help (ayuda de add)
- Documentación oficial
    -   [](https://www.vagrantup.com/docs/)[https://www.vagrantup.com/docs/](https://www.vagrantup.com/docs/)

## Inicialización: El Vagrantfile

-   Vagrantfile: describe el tipo de máquina virtual, cómo se configura y cómo se provisiona
    -   El fichero Vagrantfile:
        -   config.vm
            -   config.vm.box = “centos/7”
            -   config.vm.box_check_update = true
            -   config.vm.box_url = “http://la.box.com”
            -   config.vm.hostname = “electron”
            -   config.vm.provider = “virtualbox”
            -   config.vm.provision
            -   config.vm.synced_folders
  
  ## Boxes: las imágenes de sistemas operativos

-   Añadimos boxes con
    -   `vagrant box add ubuntu/xenial64`
    -   `vagrant box add`  [](http://url.a.la.box.com/)[http://url.a.la.box.com](http://url.a.la.box.com/)
-   Las boxes pueden soportar varios providers
    -   Virtualbox, vmware, parallels, …
-   Catálogos de boxes:
    -   [](https://app.vagrantup.com/boxes/search)[https://app.vagrantup.com/boxes/search](https://app.vagrantup.com/boxes/search)
    -   [](https://app.vagrantup.com/bento)[https://app.vagrantup.com/bento](https://app.vagrantup.com/bento)
-   Listamos boxes con
    -   `vagrant list`
-   Eliminamos boxes con
    -   `vagrant box remove ubuntu/xenial64`
-   Actualizamos el entorno vagrant (actualiza la imagen a partir de la cual se crea el entorno) con
    -   `vagrant box update`
-   Para actualizar el entorno con la box más reciente:
    -   `vagrant reload`
- Podemos crear nuestras propias boxes (continuará …)

## Comunicándonos con la máquina virtual: Directorios compartidos (synced folders)

-   Sincroniza un directorio de la máquina host en la máquina guest; trabaja con las herramientas de tu máquina y, a la vez, compilar y/o ejecuta los recursos en la máquina guest.
-   Por defecto vagrant comparte el directorio del proyecto (donde está el Vagrantfile) en el directorio  `/vagrant`
-   -   El sistema por defecto solo sincroniza cuando se hace vagrant up o vagrant reload. NO NOS VALE para desarrollar.
-   Hay que usar rsync (unidirecional)
    -   `config.vm.synced_folder ".", "/vagrant", type: "rsync"`
    -   Y lanzar en el host el comando
        -   `vagrant rsync-auto`
    -   La sincronización se lleva a cabo desde host a guest
-   O NFS o SMB (bidireccional):
    -   `config.vm.synced_folder ".", "/vagrant", type: "nfs"`
    -   Se requiere un servidor NFS (o SMB) en el host => se requiere red privada
 
 ## Comunicándonos con la máquina virtual: Acceso por red

-   Forwarded ports
    -   `config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip:`  “127.0.0.1”
-   Redes públicas
    -   `config.vm.network "public_network"`
-   Redes privadas
    -   `config.vm.network "private_network", ip: "192.168.33.10"`

##Controlando del ciclo de vida de la máquina virtual

-   Arrancando
    -   `vagrant up`
-   Recargando configuración (Vagrantfile)
    -   `vagrant reload`
-   Suspendiendo: No se para la máquina, se almacena el estado y cuando se arranca se vuelve donde se quedó. Más rápido, más memoria
    -   `vagrant suspend`
-   Parando controladamente (shutdown)
    -   `vagrant halt`
-   Destruyendo: Para y elimina todos los recursos, el sistema queda como si no se hubiera creado la máquina nunca
    -   `vagrant destroy`

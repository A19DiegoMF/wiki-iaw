# Recetas de docker

## Operaciones sobre imágenes

### Publicar una imagen en un *docker registry*:

```
docker pull <imagen>:<version>
```

### Listas imágenes

```
docker images
```

### Ver capas de una imagen

```
docker history -H <imagen>:<version>
```

### Construir una imagen

```
docker build -t <imagen>:<version> -f <dockerfile> <pathConstruccion>
```

### Borrar una imagen

```
docker rmi <imagen>:<version>
```

## Operaciones sobre contenedores

### Crear y lanzar un nuevo contenedor

```
docker run [-d] --name <nombre> -p <portHost>:<portContainer> <imagen>:<version>
```

### Ver contenedores

```
docker ps [-a] [--no-trunc]
```

### Eliminar contenedores

```
docker rm -fv <container>
```

### Ver la salida estándar de un contenedor

```
docker logs -f <container>
```

### Renombrar contenedor

```
docker rename <old-name> <new-name>
```

### Ejecución dentro de un contenedor

```
docker exec [-u <user>] [-ti] <container> <comando>
```

### Inspeccionar un contenedor

```
docker inspect <container>
```

## Dockerfile

### Instrucciones:

* `FROM`
* `RUN`
* `COPY`/`ADD`
* `ENV`
* `WORKDIR`
* `EXPOSE`
* `LABEL`
* `USER`
* `VOLUME`
* `CMD`
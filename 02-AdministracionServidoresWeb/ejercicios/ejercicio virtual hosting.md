﻿


> Written with [StackEdit](https://stackedit.io/).
## Configuración de Virtual Hosts
El objetivo es la puesta en marcha de dos sitios web utilizando el mismo servidor web apache. Hay que tener en cuenta lo siguiente:

-   Cada sitio web tendrá nombres distintos.
-   Cada sitio web compartirán la misma dirección IP y el mismo puerto (80).

-   El nombre de dominio del primero será  `www.pagina1.org`, su directorio base será  `/var/www/pagina1`  y contendrá una página llamada  `index.html`, donde se verá un mensaje de bienvenida.
-   El nombre de dominio del segundo será  `www.pagina2.org`, su directorio base será  `/var/www/pagina2`  y contendrá una página llamada  `index.html`, donde se verá un mensaje de bienvenida.

Pasos a seguir:

 1. Crear los directorio base y ficheros index anteriores.
 2. Recuerda que los ficheros servidos deben ser propiedad del usuario y grupo que usa Apache, es decir usuario  `www-data`  y grupo  `www-data`
    ```
     # chown -R www-data:www-data /var/www/pagina1
     # chown -R www-data:www-data /var/www/pagina2
    
 3. Crear los ficheros de configuración *pagina1.conf* y *pagina2.conf* de los sitios web `www.pagina1.org`, y  `www.pagina2.org`  en el directorio  `/etc/apache2/sites-available`, se puede hacer a partir del fichero del sitio web por defecto  `000-default.conf`  
    ```
     cd /etc/apache2/sites-available
     cp 000-default.conf pagina1.conf
     cp 000-default.conf pagina2.conf
  
 4.  Modificamos los ficheros  `pagina1.conf`  y  `pagina2.conf`, para indicar el nombre que vamos a usar para acceder al host virtual (`ServerName`) y el directorio de trabajo (`DocumentRoot`). También podríamos cambiar el nombre del fichero donde se guarda los logs.
*ServerName -> www.pagina1.org
Document Root -> /var/www/pagina1*
    
 5.  Habilitamos los sitios recién creados, creando un enlace simbólico a estos ficheros dentro del directorio  `/etc/apache2/sites-enabled`con los comandos:
    ```
     a2ensite pagina1
     a2ensite pagina2
    
 6.  Para terminar lo único que tendremos que hacer es cambiar el fichero hosts en los clientes y poner dos nuevas líneas donde se haga la conversión entre los dos nombre de dominio y la dirección IP del servidor.
*ip_server www.pagina1.com
ip_server www.pagina2.com*

 7. Qué pasa si accedemos al servidor con la IP? 
 8. Desactiva el sitio por defecto y accede desde el navegador con la dirección ip del servidor. Qué sucede? Qué conclusión sacas?
 10. Repite el ejercicio cambiando los directorios de trabajo a  `/srv/www`. ¿Qué modificación debes hacer ?

